package org.domsd.anch;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends Activity {

	private EditText messageInput;
	
	private Button sendMessageBtn;
	
	private ListView chatMessages;
	
	private ArrayAdapter<String> chatMessagesAdapter;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
       
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        messageInput = (EditText) findViewById(R.id.messageInput);
        sendMessageBtn = (Button) findViewById(R.id.sendMessageBtn);
        chatMessages = (ListView) findViewById(R.id.chatMessages);
        
    	chatMessagesAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1);
		
    	chatMessages.setAdapter(chatMessagesAdapter);
		
        
        sendMessageBtn.setOnClickListener(new SendMessageListener());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	
    	getMenuInflater().inflate(R.menu.main, menu);
    	return true;
    }
    
    private class SendMessageListener implements OnClickListener
    {
		@Override
		public void onClick(View v)
		{  
			String message = messageInput.getText().toString();
			
			if(!message.isEmpty())
			{
				chatMessagesAdapter.add(message);
				chatMessages.smoothScrollToPosition(chatMessagesAdapter.getCount() - 1);
				messageInput.setText("");
			}
		}
    	
    }
    

}
